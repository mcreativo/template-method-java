public class ClassA
{
    public void doSomething(boolean doStep2)
    {
        System.out.println("doing step one");
        if (doStep2)
        {
            System.out.println("doing step two");
        }
        System.out.println("doing step three");
    }
}
